export default function About(){
    return(
        <Layout page="About">
            <h1 className="text-4xl">About</h1>
            <br/>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsam facere ducimus quam hic rem, cumque eum tempore delectus ratione vitae odit dolorum molestiae sapiente, pariatur consectetur beatae distinctio eveniet cupiditate id! Perspiciatis porro assumenda officia quae repudiandae, beatae facere iusto eveniet iure molestiae ipsam quas delectus similique vero veritatis quidem? Ipsam omnis doloribus ut voluptatum est accusamus hic laudantium officiis natus voluptas mollitia reprehenderit deleniti maxime eos animi iste quam, magni ipsa eveniet optio ad ab error! Dolores, similique sit.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta itaque inventore impedit, dolor iusto ab reprehenderit enim atque nostrum cumque dolorum quibusdam quod necessitatibus accusamus corrupti possimus rem ad architecto pariatur ducimus? Facere dolore at blanditiis, et quis ut alias fuga veritatis optio temporibus aspernatur excepturi possimus, similique iure nam enim fugit provident quo praesentium odit? Tempora illo excepturi nisi. At soluta est excepturi magnam corporis ipsum unde quas quae.</p>
        </Layout>
    )
}